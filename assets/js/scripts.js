
      


// Custom Upload Button JS

$('#import_contact').change(function() {
  var i = $(this).prev('label').clone();
  var file = $('#import_contact')[0].files[0].name;
  $(this).prev('label').text(file);
});
$('#upload_excel').change(function() {
  var i = $(this).prev('label').clone();
  var file = $('#upload_excel')[0].files[0].name;
  $(this).prev('label').text(file);
});


$('.template_create_btn').on('click', function(e){
    $('.template-create-popup').toggleClass('show-popup');
    e.preventDefault();
});


// Add Class When Radio Button Checked
$('input[name=create_campaign]').click(function () {
    if (this.id == "create_campaign2") {
        $(".expand-radio").addClass('active');
    } else {
        $(".expand-radio").removeClass('active');
    }
});

$('input[name=select_media]').click(function () {
    if (this.id == "select_media1") {
        $(".voice-prompt").addClass('active');
    } else {
        $(".voice-prompt").removeClass('active');
    }
    if (this.id == "select_media2") {
        $(".call-flow").addClass('active');
    } else {
        $(".call-flow").removeClass('active');
    }
    if (this.id == "select_media3") {
        $(".direct-connect").addClass('active');
    } else {
        $(".direct-connect").removeClass('active');
    }
});


window.addEventListener('DOMContentLoaded', event => {

  // Toggle the side navigation
  const sidebarToggle = document.body.querySelector('#sidebarToggle');
  if (sidebarToggle) {
      // Uncomment Below to persist sidebar toggle between refreshes
      // if (localStorage.getItem('sb|sidebar-toggle') === 'true') {
      //     document.body.classList.toggle('sb-sidenav-toggled');
      // }
      sidebarToggle.addEventListener('click', event => {
          event.preventDefault();
          document.body.classList.toggle('sb-sidenav-toggled');
          localStorage.setItem('sb|sidebar-toggle', document.body.classList.contains('sb-sidenav-toggled'));
      });
  }

});




// Input Type Phone Number JS
$(function () {
  var code = "+91"; // Assigning value from model.
  $('#txtPhone').val(code);
  $('#txtPhone').intlTelInput({
      autoHideDialCode: true,
      autoPlaceholder: "ON",
      dropdownContainer: document.body,
      formatOnDisplay: true,
      hiddenInput: "full_number",
      initialCountry: "auto",
      nationalMode: true,
      placeholderNumberType: "MOBILE",
      preferredCountries: ['US'],
      separateDialCode: true
  });
  $('#btnSubmit').on('click', function () {
      var code = $("#txtPhone").intlTelInput("getSelectedCountryData").dialCode;
      var phoneNumber = $('#txtPhone').val();
      var name = $("#txtPhone").intlTelInput("getSelectedCountryData").name;
      alert('Country Code : ' + code + '\nPhone Number : ' + phoneNumber + '\nCountry Name : ' + name);
  });
});


// Example starter JavaScript for disabling form submissions if there are invalid fields
(() => {
  'use strict';

   const forms = document.querySelectorAll('.needs-validation');

   Array.prototype.slice.call(forms).forEach((form) => {
    form.addEventListener('submit', (event) => {
      if (!form.checkValidity()) {
        event.preventDefault();
        event.stopPropagation();
      }
      form.classList.add('was-validated');
    }, false);
  });
})();



// Jquery show hide div using checkboxes

$(document).ready(function(){
  $('input[type="checkbox"]').click(function(){
      var inputValue = $(this).attr("value");
      $("." + inputValue).toggle();
  });
});



// Bargraph Jquery 

var data1 = {
  labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct"],
  datasets: [{
    backgroundColor: "#007AFF",
    data: [25, 30, 20, 30, 25, 10, 20, 10, 20, 20],
                  
  }]
};

var options1 = {
  maintainAspectRatio: false,
  responsive: true,
  legend: {
    display: false,
   },
  scales: {
    yAxes: [{
      stacked: true,
      gridLines: {
        display: true,
        color: "rgba(100,100,100,0.1)"                  
      }
    }],
    xAxes: [{
      gridLines: {
        display: false
      }
    }]
  }
};

Chart.Bar('chart-1', {
  options: options1,
  data: data1
});



// progress-bar

$(".progress-bar").loading();
          $('input').on('click', function () {
            $(".progress-bar").loading();
          });





          new Chart(document.getElementById("bar-chart-horizontal"), {
            type: 'horizontalBar',
            data: {
              labels: ["India", "Singapore", "Australia", "Europe", "Canada"],
              datasets: [
                {
                  label: "Volume (k)",
                  backgroundColor: ["#263038","#263038","#263038","#263038","#263038"],                  
                  data: [25,20,15,10,5]
                }
              ]
            },
            options: {
              legend: { display: false }, 
              title: {
                display: true,
                text: 'By Volume'
              },
              scales: {
                yAxes: [{
                  barThickness: 15,
                  stacked: false,
                  gridLines: {
                    display: false,
                    color: "rgba(100,100,100,0.1)"                  
                  }
                }],
                xAxes: [{
                  gridLines: {
                    display: false
                  }
                }]
              }             
            }
            
        });




        new Chart(document.getElementById("bar-chart-horizontal-value"), {
          type: 'horizontalBar',
          data: {
            labels: ["India", "Singapore", "Australia", "Europe", "Canada"],
            datasets: [
              {
                label: "Value (k)",
                backgroundColor: ["#0F81FF","#0F81FF","#0F81FF","#0F81FF","#0F81FF"],                  
                data: [25,20,15,10,5]
              }
            ]
          },
          options: {
            legend: { display: false }, 
            title: {
              display: true,
              text: 'By Value'
            },
            scales: {
              yAxes: [{
                barThickness: 15,
                stacked: false,
                gridLines: {
                  display: false,
                  color: "rgba(100,100,100,0.1)"                  
                }
              }],
              xAxes: [{
                gridLines: {
                  display: false
                }
              }]
            }             
          }
          
      });



   

      